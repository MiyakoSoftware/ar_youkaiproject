﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GoToMgr2 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Invoke("goToMgr2", 90.0f);
    }

    private void goToMgr2()
    {

        SceneManager.LoadScene("IBeaconMgr");
    }
}
