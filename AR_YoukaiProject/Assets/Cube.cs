﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Cube : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("backToMain",5.0f);
	}
	
    private void backToMain()
    {
        SceneManager.LoadScene("GoToMgr");
    }
}
