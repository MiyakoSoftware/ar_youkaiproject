﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GoToMgr : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("goToMgr", 30.0f);
	}

    private void goToMgr()
    {

        SceneManager.LoadScene("IBeaconMgr");
    }
}
