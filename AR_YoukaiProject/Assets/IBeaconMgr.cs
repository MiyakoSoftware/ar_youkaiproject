﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class IBeaconMgr : MonoBehaviour {


    AudioSource audioSource;
    static bool flg = false;

    void Awake()
    {
        if (flg)
        {
            OnDestroy();
        }
    }


	// Use this for initialization
	void Start () {
        audioSource = gameObject.GetComponent<AudioSource>();

            iBeaconReceiver.BeaconRangeChangedEvent += OnBeaconRangeChanged;
            iBeaconReceiver.Scan();
        flg = true;
    }

    private void OnBeaconRangeChanged(Beacon[] beacons)
    {
        foreach (Beacon b in beacons)
        {
            switch (b.minor)
            {
                case 1:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene01", 5.0f);
                    }
                    break;

                case 2:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene02", 5.0f);
                    }
                    break;

                case 3:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene03", 5.0f);
                    }
                    break;

                case 4:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene04", 5.0f);
                    }
                    break;

                case 5:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene05", 5.0f);
                    }
                    break;

                case 6:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene06", 5.0f);
                    }
                    break;

                case 7:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene07", 5.0f);
                    }
                    break;

                case 8:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene08", 5.0f);
                    }
                    break;


                case 9:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene09", 5.0f);
                    }
                    break;

                case 10:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene10", 5.0f);
                    }
                    break;

                case 11:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene11", 5.0f);
                    }
                    break;

                case 12:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene12", 5.0f);
                    }
                    break;

                case 13:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene13", 5.0f);
                    }
                    break;

                case 14:
                    if (b.range == BeaconRange.IMMEDIATE)
                    {
                        audioSource.Play();
                        Invoke("changeScene14", 5.0f);
                    }
                    break;





                    //continue to switch syntax...
            }
        }
        return;
    }

    private void changeScene01()
    {
        SceneManager.LoadScene("Black");
    }

    private void changeScene02()
    {
        SceneManager.LoadScene("ZW");
    }

    private void changeScene03()
    {
        SceneManager.LoadScene("Skull");
    }

    private void changeScene04()
    {
        SceneManager.LoadScene("Wanyudo");
    }

    private void changeScene05()
    {
        SceneManager.LoadScene("Dog");
    }

    private void changeScene06()
    {
        SceneManager.LoadScene("Rat");
    }

    private void changeScene07()
    {
        SceneManager.LoadScene("Oni2");
    }

    private void changeScene08()
    {
        SceneManager.LoadScene("Hitotsume");
    }

    private void changeScene09()
    {
        SceneManager.LoadScene("Oni1");
    }

    private void changeScene10()
    {
        SceneManager.LoadScene("Cat");
    }

    private void changeScene11()
    {
        SceneManager.LoadScene("Blue");
    }

    private void changeScene12()
    {
        SceneManager.LoadScene("Bird");
    }

    private void changeScene13()
    {
        SceneManager.LoadScene("Kappa");
    }

    private void changeScene14()
    {
        SceneManager.LoadScene("Isogashi");
    }

    public void OnDestroy()
    {
        iBeaconReceiver.BeaconRangeChangedEvent -= OnBeaconRangeChanged;
        iBeaconReceiver.Stop();
    }
}
